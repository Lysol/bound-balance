local async = require("openmw.async")
local core = require("openmw.core")
local input = require("openmw.input")
local self = require("openmw.self")
local storage = require('openmw.storage')
local time = require("openmw_aux.time")
local types = require("openmw.types")
local ui = require("openmw.ui")
local vfs = require("openmw.vfs")
local aux_util = require("openmw_aux.util")
local I = require("openmw.interfaces")
local common = require("scripts.bound-balance.common")
local L = core.l10n(common.MOD_ID)
local slots = types.Actor.EQUIPMENT_SLOT
local settingsKey = "Settings" .. common.MOD_ID
local settingsStorage = storage.playerSection(settingsKey)
local msg = common.msg

-- Map of OAAB_Data assets we'd like to use
local OAABassets = {
    "meshes/OAAB/w/dae_scepter.nif",
    "icons/OAAB/w/daedric_scepter.dds",
    "meshes/OAAB/a/daeHelmAzura.nif",
    "icons/OAAB/a/daeHelmAzura.tga"
}

-- Map of Tamriel_Data assets we'd like to use
local TDassets = {
    "meshes/tr/a/tr_a_dae_face_sheog.nif",
    "icons/tr/a/tr_a_dae_face_sheog.dds",
    "meshes/tr/a/tr_a_dae_face_bal.nif",
    "icons/tr/a/tr_a_dae_face_bal.dds",
    "meshes/tr/a/tr_a_dae_face_dagon.nif",
    "icons/tr/a/tr_a_dae_face_dagon.dds",
    "meshes/w/w_longspear_daedric.nif",
    "icons/w/tx_longspear_daedric.dds",
    "meshes/TR/w/TR_w_daedra_lsword_02.NIF",
    "icons/TR/w/TR_w_daedra_lsword_02.dds",
    "meshes/tr/w/tr_w_daedric_bsword.nif",
    "icons/tr/w/tr_w_daedric_bsword.dds",
    "meshes/pc/w/pc_w_scamp_axe.nif",
    "textures/pc/w/pc_w_scamp_axe.dds",
    "meshes/pc/w/pc_w_scamp_mace.nif",
    "textures/pc/w/pc_w_scamp_mace.dds",
    "meshes/pc/w/pc_w_scamp_sword.nif",
    "textures/pc/w/pc_w_scamp_sword.dds"
}

local itemMap = {
    ["boundbattleaxe"] = "bound_battle_axe",
    ["bounddagger"] = "bound_dagger",
    ["boundlongbow"] = "bound_longbow",
    ["boundlongsword"] = "bound_longsword",
    ["boundmace"] = "bound_mace",
    ["boundspear"] = "bound_spear",
    ["boundboots"] = "bound_boots",
    ["boundcuirass"] = "bound_cuirass",
    ["boundgloves"] = {
        "bound_gauntlet_left",
        "bound_gauntlet_right"
    },
    ["boundhelm"] = "bound_helm",
    ["boundshield"] = "bound_shield"
}

local itemSlotMap = {
    ["bound_battle_axe"] = slots.CarriedRight,
    ["bound_dagger"] = slots.CarriedRight,
    ["bound_longbow"] = slots.CarriedRight,
    ["bound_longsword"] = slots.CarriedRight,
    ["bound_mace"] = slots.CarriedRight,
    ["bound_spear"] = slots.CarriedRight,
    ["bound_boots"] = slots.Boots,
    ["bound_cuirass"] = slots.Cuirass,
    ["bound_helm"] = slots.Helmet,
    ["bound_shield"] = slots.CarriedLeft,
}

-- Map items to their replacements
local replaceMap = {
    ["boundbattleaxe"] = "momw_bb_bound_battle_axe_%s",
    ["bounddagger"] = "momw_bb_bound_dagger_%s",
    ["boundlongbow"] = "momw_bb_bound_longbow_%s",
    ["boundlongsword"] = "momw_bb_bound_longsword_%s",
    ["boundmace"] = "momw_bb_bound_mace_%s",
    ["boundspear"] = "momw_bb_bound_spear_%s",
    ["boundboots"] = "momw_bb_bound_boots_%s",
    ["boundcuirass"] = "momw_bb_bound_cuirass_%s",
    ["boundgloves"] = {
        "momw_bb_bound_gauntletl_%s",
        "momw_bb_bound_gauntletr_%s"
    },
    ["boundhelm"] = "momw_bb_bound_helm_%s",
    ["boundshield"] = "momw_bb_bound_shield_%s"
}

local activeEffects = {}
local activeSpells = {}
local equippedGear = {}

local haveOAAB = true
for _, OAABasset in pairs(OAABassets) do
    if not vfs.fileExists(string.lower(OAABasset)) then
        haveOAAB = false
    end
end

if haveOAAB then
    print(L("haveOAAB"))
else
    print(L("noHaveOAAB"))
end

local haveTD = true
for _, TDasset in pairs(TDassets) do
    if not vfs.fileExists(string.lower(TDasset)) then
        haveTD = false
    end
end

if haveTD then
    print(L("haveTD"))
else
    print(L("noHaveTD"))
end

I.Settings.registerPage {
    key = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "page_name",
    description = "description"
}

I.Settings.registerGroup {
    key = settingsKey,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "options_name",
    permanentStorage = false,
    settings = {
        {
            key = "useExtraAssets",
            name = "useExtraAssets_name",
            description = "useExtraAssets_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "useTwAssets",
            name = "useTwAssets_name",
            description = "useTwAssets_desc",
            default = false,
            renderer = "checkbox"
        },
        {
            key = "reEquip",
            name = "reEquip_name",
            description = "reEquip_desc",
            default = true,
            renderer = "checkbox"
        },
        {
            key = "levelLimit",
            name = "levelLimit_name",
            description = "levelLimit_desc",
            default = "6",
            argument = {
                l10n = common.MOD_ID,
                items = {"1", "2", "3", "4", "5", "6"}
            },
            renderer = "select"
        },
        {
            key = "showMessages",
            name = "showMessages_name",
            description = "showMessages_desc",
            default = true,
            renderer = "checkbox"
        }
    }
}

local function getSlot(itemRecordId)
    return itemSlotMap[itemRecordId]
end

local function getReplacementLevel(effectId)
    local baseConj = self.type.stats.skills.conjuration(self).base
    local levelLimit = tonumber(settingsStorage:get("levelLimit"))
    local level = "i"
    local useExtraAssets = settingsStorage:get("useExtraAssets")
    local useTwAssets = settingsStorage:get("useTwAssets")
    if useTwAssets
        and (string.match(effectId, "axe")
             or string.match(effectId, "dagger")
             or string.match(effectId, "longbow")
             or string.match(effectId, "longsword")
             or string.match(effectId, "mace")
             or string.match(effectId, "spear")) then
        return level .. "_tw"
    elseif haveTD and useExtraAssets
        and (string.match(effectId, "axe")
             or string.match(effectId, "mace")
             or string.match(effectId, "dagger")
             or string.match(effectId, "helm")) then
        level = level .. "_td"
    end
    if levelLimit == 6 and baseConj >= 100 then
        level = "vi"
        if haveTD and useExtraAssets
            and (string.match(effectId, "spear") or string.match(effectId, "longsword")) then
            level = level .. "_td"
        end
        if haveOAAB and (string.match(effectId, "helm") or string.match(effectId, "mace")) then
            level = level .. "_oaab"
        end
    elseif (levelLimit == 5 and baseConj >= 80) or (baseConj >= 80 and baseConj < 100) then
        level = "v"
        if haveTD and useExtraAssets
            and (string.match(effectId, "spear") or string.match(effectId, "longsword")) then
            level = level .. "_td"
        end
        if haveOAAB and useExtraAssets
            and (string.match(effectId, "helm") or string.match(effectId, "mace")) then
            level = level .. "_oaab"
        end
    elseif (levelLimit == 4 and baseConj >= 60) or (baseConj >= 60 and baseConj < 100) then
        level = "iv"
        if haveTD and useExtraAssets
            and (string.match(effectId, "longsword") or string.match(effectId, "helm")) then
            level = level .. "_td"
        end
    elseif (levelLimit == 3 and baseConj >= 40) or (baseConj >= 40 and baseConj < 60) then
        level = "iii"
        if haveTD and useExtraAssets and string.match(effectId, "helm") then
            level = level .. "_td"
        end
    elseif (levelLimit == 2 and baseConj >= 20) or (baseConj >= 20 and baseConj < 40) then
        level = "ii"
        if haveTD and useExtraAssets
            and (string.match(effectId, "axe")
                 or string.match(effectId, "mace")
                 or string.match(effectId, "dagger")) then
            level = level .. "_td"
        end
    end
    return level
end

-- Cache the value for this so it isn't checked in onUpdate
local reEquip = settingsStorage:get("reEquip")
local function updateReEquip(_, key)
    if key == "reEquip" then
        reEquip = settingsStorage:get("reEquip")
    end
end
settingsStorage:subscribe(async:callback(updateReEquip))

local function readSpells()
    for _, spell in pairs(self.type.activeSpells(self)) do
        local spellId = spell.id
        local newItems = {}
        local oldItems = {}
        for _, effect in pairs(spell.effects) do
            local effectId = effect.id
            if replaceMap[effectId] and not activeEffects[effectId] then
                if I.ShieldUnequipper ~= nil then
                    I.ShieldUnequipper.Disable()
                end
                msg(string.format("Spell %s has a valid effect: %s", spellId, effectId))
                activeEffects[effectId] = {}
                if activeSpells[spellId] == nil then
                    activeSpells[spellId] = {}
                end
                activeSpells[spellId][effectId] = true
                activeEffects[effectId] = {}
                activeEffects[effectId].duration = effect.duration
                activeEffects[effectId].started = core.getSimulationTime()
                activeEffects[effectId].equipped = {}
                activeEffects[effectId].items = {}
                activeEffects[effectId].slots = {}
                local level = getReplacementLevel(effectId)
                --TODO: Make all of them tables?
                if effectId == "boundgloves" then
                    local oldLeft = self.type.inventory(self):find(itemMap[effectId][1])
                    local oldRight = self.type.inventory(self):find(itemMap[effectId][2])
                    table.insert(oldItems, oldLeft)
                    table.insert(oldItems, oldRight)
                    local newLeft = string.format(replaceMap[effectId][1], level)
                    local newRight = string.format(replaceMap[effectId][2], level)
                    newItems[newLeft] = {durationLeft = effect.durationLeft, effectId = effectId, slot = slots.LeftGauntlet}
                    newItems[newRight] = {durationLeft = effect.durationLeft, effectId = effectId, slot = slots.RightGauntlet}
                    table.insert(activeEffects[effectId].items, newLeft)
                    table.insert(activeEffects[effectId].items, newRight)
                    activeEffects[effectId].slots = {
                        [slots.LeftGauntlet] = true,
                        [slots.RightGauntlet] = true
                    }
                else
                    local oldItem = self.type.inventory(self):find(itemMap[effectId])
                    table.insert(oldItems, oldItem)
                    local newItem = string.format(replaceMap[effectId], level)
                    local slot = getSlot(oldItem.recordId)
                    newItems[newItem] = {durationLeft = effect.durationLeft, effectId = effectId, slot = slot}
                    table.insert(activeEffects[effectId].items, newItem)
                    activeEffects[effectId].slots = {[slot] = true}
                end
            end
        end
        if next(oldItems) ~= nil and next(newItems) ~= nil then
            msg("Sending payload to global script")
            core.sendGlobalEvent(
                "momw_bb_handleItems",
                {player = self, newItems = newItems, oldItems = oldItems}
            )
        end
    end
end

local equipping = false
local function tryToReEquip(equipmentSlots)
    while equipping do msg("Waiting to equip...") end
    msg("Trying to re-equip " .. #equipmentSlots .. " slots")
    equipping = true
    local equipped = self.type.equipment(self)
    for _, slot in pairs(equipmentSlots) do
        local thing = equippedGear[slot]
        if thing and thing:isValid() then
            msg("Re-equipping: " .. thing.recordId)
            equipped[slot] = thing
        end
    end
    self.type.setEquipment(self, equipped)
    equipping = false
    msg("Re-equip done")
end

local function equipItems(items)
    local equipment = self.type.equipment(self)
    for slot, data in pairs(items) do
        msg(string.format("Equipping: %s", data.item.recordId))
        equipment[slot] = data.item
        table.insert(activeEffects[data.effectId].equipped, data.item)
    end
    self.type.setEquipment(self, equipment)
end

local function expired(data)
    local s = {}
    if data.effectIds == nil then
        msg("Expired with nil effectIds!")
        return
    end
    for _, effectId in pairs(data.effectIds) do
        if activeEffects[effectId] then
            msg(string.format("Removing from active effects: %s", effectId))
            self.type.activeEffects(self):remove(effectId)
            for slot, _ in pairs(activeEffects[effectId].slots) do
                table.insert(s, slot)
            end
            activeEffects[effectId] = nil
            for spell, effects in pairs(activeSpells) do
                for effect, _ in pairs(effects) do
                    if effect == effectId then
                        msg(string.format("Removing from active spell effects: %s", effect))
                        activeSpells[spell][effect] = nil
                    end
                end
                if next(activeSpells[spell]) == nil then
                    msg(string.format("Removing from active spells: %s", spell))
                    activeSpells[spell] = nil
                end
            end
        end
    end
    if reEquip and next(s) then
        tryToReEquip(s)
    end
end

local function detectDispel()
    -- ... or try to!
    local toExpire = {}
    for spell, data in pairs(activeSpells) do
        if self.type.activeSpells(self):isSpellActive(spell) == false then
            local toDelete = {}
            for effectId, _ in pairs(data) do
                if (core.getSimulationTime() - activeEffects[effectId].started) >= activeEffects[effectId].duration then
                    -- This was probably a normal expiry
                    goto tryNext
                end
                for _, item in pairs(activeEffects[effectId].equipped) do
                    table.insert(toDelete, item)
                end
                table.insert(toExpire, effectId)
                ::tryNext::
            end
            if next(toDelete) then
                msg("Probable dispel effect detected, despawning all the things!")
                core.sendGlobalEvent("momw_bb_disableItems", toDelete)
            end
        end
    end
    if next(toExpire) then
        msg("Expiring " .. #toExpire .. " effects")
        expired(toExpire)
    end
end

-- THANKS: https://stackoverflow.com/a/22831842
local function stringStartsWith(string, start)
   return string.sub(string, 1, string.len(start)) == start
end

local function noticeEquippedGear()
    if reEquip then
        msg("Noticing equipped gear")
        for slot, thing in pairs(self.type.getEquipment(self)) do
            if not stringStartsWith(thing.recordId, "momw_bb_") then
                msg("Noticing: " .. thing.recordId)
                equippedGear[slot] = thing
            end
        end
    end
end

local function onPlayerAdded()
    noticeEquippedGear()
end

local restStart
local function UiModeChanged(data)
    local toExpire = {}
    if ((data.oldMode == "Interface" and data.newMode == nil)
        or (data.oldMode == "Barter" and data.newMode == "Dialogue")) then
        noticeEquippedGear()
    end
    --TODO: Should we check for "dropped" items after Barter?
    if (data.oldMode == nil and data.newMode == "Rest")
        or (data.oldMode == "Dialogue" and data.newMode == "Training")
        or (data.oldMode == "Dialogue" and data.newMode == "Travel") then
        restStart = core.getGameTime()
    end
    if (data.oldMode == "Rest" and data.newMode == nil)
        or (data.oldMode == "Training" and data.newMode == "Dialogue")
        or (data.oldMode == "Travel" and data.newMode == "Dialogue") then
        local duration = core.getGameTime() - restStart
        if duration < 1 then return end
        -- Rest, Training, or Travel was finished, get the time and check active items to expire
        for effectId, eData in pairs(activeEffects) do
            if duration > eData.duration then
                for _, item in pairs(eData.equipped) do
                    msg(string.format("Expired due to rest, training, or travel: %s", item.recordId))
                    core.sendGlobalEvent("momw_bb_disableItems", {item})
                end
                table.insert(toExpire, effectId)
            end
        end
    end
    -- Detect item drops
    if (data.oldMode == "Container" or data.oldMode == "Interface") and data.newMode == nil then
        for effectId, eData in pairs(activeEffects) do
            for i, item in pairs(eData.equipped) do
                if self.type.inventory(self):countOf(item.recordId) < 1 then
                    core.sendGlobalEvent("momw_bb_disableItems", {item})
                    eData.equipped[i] = nil
                    if settingsStorage:get("showMessages") then
                        ui.showMessage(L("dropped"))
                    end
                    if #eData.equipped < 1 then
                        msg(string.format("Removing effect because its item was dropped: %s", effectId))
                        table.insert(toExpire, effectId)
                    end
                end
            end
        end
    end
    if next(toExpire) then
        msg("Expiring " .. #toExpire .. " effects")
        expired(toExpire)
    end
end

local function onLoad(data)
    if not data then return end
    activeEffects = data.activeEffects or {}
    activeSpells = data.activeSpells or {}
    equippedGear = data.equippedGear or {}
end

local function onSave()
    return {
        activeEffects = activeEffects,
        activeSpells = activeSpells,
        equippedGear = equippedGear,
        scriptVersion = common.scriptVersion
    }
end

local function update()
    readSpells()
    detectDispel()
end

time.runRepeatedly(
    update,
    common.updateInterval,
    {
        initialDelay = 1,
        type = time.SimulationTime
    }
)

local function onKeyPress(key)
    local F1 = 58 -- key.code
    local numbers = {
        ["1"] = true, ["2"] = true, ["3"] = true, ["4"] = true, ["5"] = true,
        ["6"] = true, ["7"] = true, ["8"] = true, ["9"] = true, ["0"] = true
    }
    --TODO: This is a deprecated function but it's also the
    --TODO: only way I know to check if an action happened....
    if input.isActionPressed(input.ACTION.ToggleSpell)
        or (key.code == F1 or numbers[key.symbol]) then
        noticeEquippedGear()
    end
    if common.debugMsgs() and key.code == 29 then
        print("===============================================")
        print("===============================================")
        print("===============================================")
        print("===============================================")
        print("==== ACTIVE EFFECTS:")
        print(aux_util.deepToString(activeEffects, 4))
        print("==== ACTIVE SPELLS:")
        print(aux_util.deepToString(activeSpells, 4))
        print("==== EQUIPPED GEAR:")
        print(aux_util.deepToString(equippedGear, 4))
        print("===============================================")
        print("===============================================")
        print("===============================================")
        print("===============================================")
        for _, spell in pairs(self.type.activeSpells(self)) do print(spell) end
    end
end

return {
    engineHandlers = {
        onKeyPress = onKeyPress,
        onLoad = onLoad,
        onSave = onSave
    },
    eventHandlers = {
        momw_bb_equipItems = equipItems,
        momw_bb_expired = expired,
        momw_bb_onPlayerAdded = onPlayerAdded,
        UiModeChanged = UiModeChanged
    }
}
