local time = require("openmw_aux.time")
local world = require("openmw.world")
local I = require("openmw.interfaces")
local common = require("scripts.bound-balance.common")
local msg = common.msg

I.Settings.registerGroup {
    key = "SettingsGlobal" .. common.MOD_ID,
    page = common.MOD_ID,
    l10n = common.MOD_ID,
    name = "globalSettingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = "debugMsgs",
            name = "debugMsgs_name",
            description = "debugMsgs_desc",
            default = false,
            renderer = "checkbox"
        }
    }
}

local function disableItems(items)
    for _, item in pairs(items) do
        if item.count > 0 then
            msg(string.format("Despawning: %s", item.recordId))
            item:remove(1)
        end
    end
end

local timerCallback = time.registerTimerCallback(
    "momw_bb_timerCallback",
    function(data)
        local effectIds = {}
        local toDespawn = {}
        local retrySlots = {}
        for _, thing in pairs(data.toDespawn) do
            if thing.item:isValid() then
                table.insert(toDespawn, thing.item)
            end
            table.insert(effectIds, thing.effectId)
            table.insert(retrySlots, thing.slot)
            msg(string.format("Expiring: %s", thing.item.recordId))
        end
        disableItems(toDespawn)
        data.player:sendEvent(
            "momw_bb_expired",
            {
                effectIds = effectIds,
                slots = retrySlots
            }
        )
    end
)

local function handleItems(data)
    disableItems(data.oldItems)
    local forPlayer = {}
    local toDespawn = {}
    local durationLeft
    for item, iData in pairs(data.newItems) do
        local newItem = world.createObject(item)
        newItem:moveInto(data.player.type.inventory(data.player))
        forPlayer[iData.slot] = {effectId = iData.effectId, item = newItem}
        if durationLeft == nil then
            durationLeft = iData.durationLeft
        end
        table.insert(
            toDespawn,
            {effectId = iData.effectId,
             item = newItem,
             slot = iData.slot
        })
        msg(string.format("Spawned: %s", newItem.recordId))
    end
    time.newSimulationTimer(
        -- Ignore the fact that some time passed in between when we
        -- recorded durationLeft and now, this will help ensure that the
        -- related spell effect won't be active after our new item expires.
        durationLeft,
        timerCallback,
        {player = data.player, toDespawn = toDespawn}
    )
    msg("Sending payload to player")
    data.player:sendEvent("momw_bb_equipItems", forPlayer)
end

return {
    engineHandlers = {
        onPlayerAdded = function(player)
            player:sendEvent("momw_bb_onPlayerAdded")
        end
    },
    eventHandlers = {
        momw_bb_disableItems = disableItems,
        momw_bb_handleItems = handleItems
    }
}
