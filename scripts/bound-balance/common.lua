local async = require("openmw.async")
local core = require("openmw.core")
local storage = require('openmw.storage')
local MOD_ID = "BoundBalance"
local globalSettingsKey = "SettingsGlobal" .. MOD_ID
local globalStorage = storage.globalSection(globalSettingsKey)

local errors = {}
if core.API_REVISION < 59 then
    table.insert(errors, core.l10n(MOD_ID)("needNewerOpenMW"))
else
    if not core.contentFiles.has("bound-balance.omwaddon") then
        table.insert(errors, core.l10n(MOD_ID)("needOmwaddon"))
    end
end

if #errors > 0 then
    print("!!!!!!!!!!ERRORS!!!!!!!!!!")
	print("Bound Balance had errors loading:")
    for _, err in pairs(errors) do
        print(err)
    end
    error("Could not load Bound Balance due to errors!")
end

local debugMsgs = globalStorage:get("debugMsgs")
local function updateDebugMsgs(_, key)
    if key == "debugMsgs" then
        debugMsgs = globalStorage:get("debugMsgs")
    end
end
globalStorage:subscribe(async:callback(updateDebugMsgs))

local function msg(...)
    if debugMsgs then
        print(string.format("[%s]: %s", MOD_ID, ...))
    end
end

return {
    debugMsgs = function()
        return debugMsgs
    end,
    MOD_ID = MOD_ID,
    msg = msg,
    updateInterval = .1,
    scriptVersion = 1
}
