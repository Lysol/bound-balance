## Bound Balance Changelog

#### 2.5

* Added support for assets from Trainwiz's [Bound Weapon Replacer](https://www.nexusmods.com/morrowind/mods/41778) ([#5](https://gitlab.com/modding-openmw/bound-balance/-/issues/5))

<!-- [Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/#TODO) -->

#### 2.4

* The `onUpdate` engine handler is no longer used, instead this mod runs on a very short timer
    * The interval is one tenth of a second which should amount to much less work than using `onUpdate` on average
    * In my testing it's basically good enough; there tends to be a bit more of a lag between deleting the vanilla bound item and inserting our own, but generally it works without issues
* Handle when Dispel is cast on the player with active bound effects managed by this mod
* Fixed a bug that prevented items from being re-equipped ([#4](https://gitlab.com/modding-openmw/bound-balance/-/issues/4))
* Optimized re-equip code ([#4](https://gitlab.com/modding-openmw/bound-balance/-/issues/4))
* Added a test plugin (not intended for normal gameplay usage!)

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/33778197)

#### 2.3

* Bound items are now despawned when you drop them with the Container menu open
* Fixed a possible error when trying to re-equip a now nonexisting item

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/26152636)

#### 2.2

* Fixed a problem with loading the latest version from an older version

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/25127941)

#### 2.1

* Added DE localization
* Better support for [Shield Unequipper](https://modding-openmw.gitlab.io/shield-unequipper/)

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/24977884)

#### 2.0

* **If you're upgrading from a previous version please make sure any bound items are expired when loading your save!**
* Rewrote the mod to be compatible with the latest API changes
* Optimized player and global scripts
* Fixed a bug where spell effects were not properly cleared
    * This is possible because of API changes in OpenMW itself, so credit goes to the OpenMW team for this

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/24374348)

#### 1.1

* Fix bug with re-equip that prevented any until all bound items were gone
* Handle Training
* Handle scripted passing of time that affects magic spells and effects with a duration
* Properly registered all timer callbacks to avoid edge-case bugs

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/21556076)

#### 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/signpost-fast-travel/-/packages/21515400)
